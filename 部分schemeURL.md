## 代码
```
home: "snssdk1128://feed?refer=web&gd_label={{gd_label}}",
detail: "snssdk1128://aweme/detail/{{id}}?refer=web&gd_label={{gd_label}}&appParam={{appParam}}&needlaunchlog=1",
user: "snssdk1128://user/profile/{{uid}}?refer=web&gd_label={{gd_label}}&type={{type}}&needlaunchlog=1",
challenge: "snssdk1128://challenge/detail/{{id}}?refer=web",
music: "snssdk1128://music/detail/{{id}}?refer=web",
live: "snssdk1128://live?room_id={{room_id}}&user_id={{user_id}}&from=webview&refer=web",
webview: "snssdk1128://webview?url={{url}}&from=webview&refer=web",
webview_fullscreen: "snssdk1128://webview?url={{url}}&from=webview&hide_nav_bar=1&refer=web",
poidetail: "snssdk1128://poi/detail?id={{id}}&from=webview&refer=web",
forward: "snssdk1128://forward/detail/{{id}}",
billboard_word: "snssdk1128://search/trending",
billboard_video: "snssdk1128://search/trending?type=1",
billboard_music: "snssdk1128://search/trending?type=2",
billboard_positive: "snssdk1128://search/trending?type=3",
billboard_star: "snssdk1128://search/trending?type=4"
```

## 例子(部分参数可省略)

跳转主页并关注:
`snssdk1128://user/profile/72673737181?refer=web&gd_label=click_wap_profile_bottom&type=need_follow&needlaunchlog=1`
`snssdk1128://user/profile/72673737181?refer=web&gd_label=click_wap_download_follow&type=need_follow&needlaunchlog=1`

打开视频：
`snssdk1128://aweme/detail/6683443624597916941?refer=web&gd_label=click_wap_profile_feature&appParam=&needlaunchlog=1`

原声（同一个音乐的作品）：
`snssdk1128://music/detail/6680045787365247747?refer=web`

热搜榜：
`snssdk1128://search/trending`

最热视频: 
`snssdk1128://search/trending?type=1`

音乐榜: 
`snssdk1128://search/trending?type=2`

热搜（正能量）: 
`snssdk1128://search/trending?type=3`

明星爱豆榜:
 `snssdk1128://search/trending?type=4`

抖音内打开网址: 
`snssdk1128://webview?url=http%3A%2F%2Fbaidu.com&from=webview&refer=web`

抖音内打开网址（全屏）: 
`snssdk1128://webview?url=http%3A%2F%2Fbaidu.com&from=webview&hide_nav_bar=1&refer=web`


## AutoJs调用方法：
```
"auto";
const _start_app = function() {
    app.startActivity({        
      action: "VIEW",
      data: "snssdk1128://webview?url=http%3A%2F%2Fbaidu.com&from=webview&refer=web", 
    });
  }
  _start_app();
```
## 按键精灵调用方法：
```
Import "ShanHai.lua"

//RunApp "com.ss.android.ugc.aweme"
ShanHai.execute("am start -a android.intent.action.VIEW  -d snssdk1128://aweme/detail/6682674534149000456")
//TracePrint ShanHai.GetTopActivity()
```
